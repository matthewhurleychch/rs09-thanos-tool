package const

object Archives {

    /**
     * Archives that are located in the CONFIGURATION index (2)
     */
    const val FLOOR_UNDERLAYS = 1
    const val FLOOR_OVERLAYS = 4
}